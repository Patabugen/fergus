"use strict"
require('dotenv').config();
var LightwaveRF = require("lightwaverf");
const express = require('express');
const app = express();

var devices = {
	'kitchen_light' : {
		'device': 1,
		'room' : 2
	}
}

app.get('/', function (req, res) {
	var output = [];
	output.push("<h1>Options are:</h1>");
	output.push("<ul>");
	output.push("<li>/light/dim/:device/:level</li>");
	output.push("<li>/light/on/:device</li>");
	output.push("<li>/light/on/:device</li>");
	output.push("</ul>");
	res.send(output.join("\n"));
});

app.get('/light/on/:device', function (req, res) {
	var device = req.params.device;
	if ( undefined == devices[device]) {
		res.send("Error: Unknown Device" + device);
	}
	lw.turnDeviceOn(devices[device].room, devices[device].device, function(error, content) {
		if (error) {
			res.send("Error: " + error.message);
		}
		res.send('OK')
	});

});

app.get('/light/dim/:device/:level', function (req, res) {
	var device = req.params.device;
	var level = req.params.level;
	if ( undefined == devices[device]) {
		res.send("Error: Unknown Device" + device);
	}
	lw.setDeviceDim(devices[device].room, devices[device].device, level, function(error, content) {
		if (error) {
			res.send("Error: " + error.message);
		}
		res.send('OK')
	});

});


app.get('/light/off/:device', function (req, res) {
	var device = req.params.device;
	if ( undefined == devices[device]) {
		res.send("Error: Unknown Device" + device);
	}
	lw.turnDeviceOff(devices[device].room, devices[device].device, function(error, content) {
		if (error) {
			res.send("Error: " + error.message);
		}
		res.send('OK')
	});

});

app.listen(process.env.http_port, function () {
  console.log('Farlge House Control Listening on port ' + process.env.http_port + '!')
});


var lw = new LightwaveRF({
	ip: process.env.lightwave_ip,
	email: process.env.lightwave_email,
	pin: process.env.lightwave_pin
});

lw.register(function(){
	console.log("I'm Registered!");
});

var timeout = 1;
var room = 1;
var device = 1;

setTimeout(function(){
//	lw.sendUdp('!R2D1F1');
//	lw.turnDeviceOff(2, 1, function(error, content) {
//		if (error) {
//			console.log("Error turning device off " + error.message);
//		}
//	});
	// turnOff(room, device);
}, 1000);
